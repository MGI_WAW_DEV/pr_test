﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CashManagement.Application.Interfaces;
using CashManagement.Domain.Entities;

namespace CashManagement.Infrastructure.PartnerHierarchy
{
    public class PartnerHierarchyDataProvider : IPartnerHierarchyDataProvider
    {
        public Task<Domain.Entities.Partner> GetPartner(string oracleAccountNumber)
        {
            throw new NotImplementedException();

            PartnerDataClient pdc = new PartnerDataClient(new HttpClient());
            var partnerData = pdc.GetPartnerForOracleAccountNumberUsingGETAsync(oracleAccountNumber);
        }
    }
}
