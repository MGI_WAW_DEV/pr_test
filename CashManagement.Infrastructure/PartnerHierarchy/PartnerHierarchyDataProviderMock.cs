﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CashManagement.Application.Interfaces;
using CashManagement.Domain.Entities;
using Newtonsoft.Json;

namespace CashManagement.Infrastructure.PartnerHierarchy
{
    public class PartnerHierarchyDataProviderMock : IPartnerHierarchyDataProvider
    {
        private string data =
            "{  \"Partner\": {    \"hqOracleAccountNumber\": \"100000041\",    \"country\": \"USA\",    \"address\": \"1550 UTICA AVE S\",    \"city\": \"SAINT LOUIS PARK\",    \"postalCode\": \"55416\",    \"currentLevel\": \"HQ\",    \"partnerNumber\": \"500000065\",    \"parentOracleAccountNumber\": \"100000041\",    \"phone\": \"\",    \"name\": \"MGI HR\",    \"partnerId\": \"140407\",    \"state\": \"MN\",    \"oracleAccountNumber\": \"100000041\",    \"status\": \"Active\"  }}";
        public Task<Domain.Entities.Partner> GetPartner(string oracleAccountNumber)
        {
            return Task.FromResult(JsonConvert.DeserializeObject<Domain.Entities.Partner>(data));
        }
    }
}
