﻿using CashManagement.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Infrastructure
{
    public class UserClaimsProvider : IUserClaimsProvider
    {
        public string GetClaimValue(string key)
        {
            switch (key)
            {
                case "LocationId":
                    return "67610501";
                case "CountryId":
                    return "BEL";
                case "EmployeeNumber":
                    return "CAT7";
                default:
                    return String.Empty;
            }
        }
    }
}
