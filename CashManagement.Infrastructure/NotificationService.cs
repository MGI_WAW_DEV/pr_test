﻿using CashManagement.Application.Interfaces;
using CashManagement.Application.Notifications.Models;
using System.Threading.Tasks;

namespace CashManagement.Infrastructure
{
    public class NotificationService : INotificationService
    {
        public Task SendAsync(Message message)
        {
            return Task.CompletedTask;
        }
    }
}
