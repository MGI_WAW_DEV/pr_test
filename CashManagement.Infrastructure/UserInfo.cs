﻿using CashManagement.Common;
using CashManagement.Persistence.Infrastructure;
using CashManagement.Persistence.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Infrastructure
{
    public class UserInfo : IUserInfo
    {
        private readonly IOrganizationSelector _selector;
        private readonly IUserClaimsProvider _provider;
        private LocationMapping _mapping;
        public UserInfo(IOrganizationSelector selector, IUserClaimsProvider provider)
        {
            _selector = selector;
            _provider = provider;
            _mapping = _selector.GetMapping();
            SetValues();
        }

        public string OrganizationId { get; private set; }

        public string AgencyNumber { get; private set; }
        public string EmployeeNumber { get; private set; }


        private void SetValues()
        {
            OrganizationId = _mapping.OrganizationId;
            AgencyNumber = _mapping.AgencyNumber;
            EmployeeNumber = _provider.GetClaimValue("EmployeeNumber");
        }

    }
}
