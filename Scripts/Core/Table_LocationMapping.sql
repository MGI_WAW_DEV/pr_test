IF OBJECT_ID('[dbo].[LocationMapping]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[LocationMapping]; 


CREATE TABLE [dbo].[LocationMapping](
	[OrganizationId] [varchar](15) NULL,
	[LocationId] [varchar](15) NOT NULL,
	[CountryId] [varchar](15) NOT NULL,
	[AgencyNumber] [varchar](10) NOT NULL,
 CONSTRAINT [PK_LocationMapping] PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC,
	[CountryId] ASC
))


INSERT INTO [dbo].[LocationMapping] ([OrganizationId],[LocationId],[CountryId],[AgencyNumber])
VALUES('BELCORE','67610501','BEL','AMIRA')
