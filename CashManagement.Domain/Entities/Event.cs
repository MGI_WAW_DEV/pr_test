﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Domain.Entities
{
    public class Event
    {
        public string AgencyNumber { get; set; }
        public string TimeKey { get; set; }
        public string ContactNumber { get; set; }
        public string EmployeeNumber { get; set; }
        public string DeskNumber { get; set; }
        public DateTime TransTime { get; set; }
        public short EventID { get; set; }
        public string Report { get; set; }
        public short Reviewed { get; set; }
        public DateTime ReviewDate { get; set; }
        public string ReviewEmployee { get; set; }
        public string OrganizationId { get; set; }


    }
}
