﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Domain.Entities
{
    public class DeskCLREM
    {
        public string DeskNumber {get;set;}
        public string AgencyNumber { get; set; }
        public DateTime Closed { get; set; }
        public string EmployeeNumber { get; set; }
        public string Remarks { get; set; }
        public string OrganizationId { get; set; }
    }
}
