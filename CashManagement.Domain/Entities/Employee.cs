﻿using System;
using System.Collections.Generic;

namespace CashManagement.Domain.Entities
{
    public class Employee
    {
        public string EmployeeNumber { get; set; }
        public string OrganizationId { get; set; }
        
        public string Name { get; set; }
        
    }
}
