﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Domain.Entities
{
    public class DeskBalance
    {
        public string DeskNumber {get; set;}
        public string AgencyNumber {get; set;}
        public string Currency {get; set;}
        public double IBalCur {get; set;}
        public decimal IBalVal {get; set;}
        public double PBalCur {get; set;}
        public decimal PBalVal {get; set;}
        public double SBalCur {get; set;}
        public decimal SBalVal {get; set;}
        public short LastStatus {get; set;}
        public string LastRemarks {get; set;}
        public string LastEmployee {get; set;}
        public DateTime LastClosed {get; set;}
        public decimal CouponBalVal {get; set;}
        public string OrganizationId {get; set;}
        public decimal SPRunCom {get; set;}
        public decimal SPRunComTax {get; set;}

    }
}
