﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Domain.Entities
{
    public class DeskOpenClose
    {
        public string AgencyNumber { get; set; }
        public string DeskNumber { get; set; }
        public string Currency { get; set; }
        public string TimeStamp { get; set; }
        public string OC { get; set; }
        public DateTime Date { get; set; }
        public string Employee { get; set; }
        public string Remarks { get; set; }
        public double BalCur { get; set; }
        public decimal BalVal { get; set; }
        public double DBalCur { get; set; }
        public decimal DBalVal { get; set; }
        public string OrganizationId { get; set; }
        public int ThirdPartyTransactions { get; set; }
        public decimal ThirdPartyAmount { get; set; }
    }
}
