﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Domain.Entities
{
    public class Desk
    {
        public string DeskNumber { get; set; }
        public string AgencyNumber { get; set; }
        public string Name { get; set; }
        public string CurrentEmployee { get; set; }
        public DateTime CurrentAcceptTime { get; set; }
        public string LastEmployeeId { get; set; }
        
        public DateTime LastClosed { get; set; }
        public short LastStatus { get; set; }
        public string LastRemarks { get; set; }

        public string OrganizationId { get; set; }


        public Employee LastEmployee { get; set; }
    }
}
