﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Domain.Entities
{
    public class Partner
    {
            public string Address { get; set; }
            public string City { get; set; }
            public string Country { get; set; }
            public string CurrentLevel { get; set; }
            public string HqOracleAccountNumber { get; set; }
            public string Name { get; set; }
            public string OracleAccountNumber { get; set; }
            public string ParentOracleAccountNumber { get; set; }
            public string PartnerId { get; set; }
            public string PartnerNumber { get; set; }
            public string PostalCode { get; set; }
            public string State { get; set; }
            public string Status { get; set; }
    }
}
