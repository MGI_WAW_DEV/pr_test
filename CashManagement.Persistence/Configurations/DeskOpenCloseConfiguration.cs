﻿using CashManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CashManagement.Persistence.Configurations
{
    public class DeskOpenCloseConfiguration : IEntityTypeConfiguration<DeskOpenClose>
    {
        public void Configure(EntityTypeBuilder<DeskOpenClose> builder)
        {
            builder.HasKey(d => new { d.AgencyNumber, d.DeskNumber, d.Currency, d.TimeStamp, d.OC, d.OrganizationId});
            builder.Property(d => d.ThirdPartyTransactions).HasColumnName("THIRDPARTY_TRANSACTIONS");
            builder.Property(d => d.ThirdPartyAmount).HasColumnName("THIRDPARTY_AMOUNT");
            
            builder.ToTable("FX_DESKOPENCLOSE");
        }
    }
}
