﻿using CashManagement.Persistence.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CashManagement.Persistence.Configurations
{
    public class LocationMappingConfiguration : IEntityTypeConfiguration<LocationMapping>
    {
        public void Configure(EntityTypeBuilder<LocationMapping> builder)
        {
            builder.HasKey(table => new { table.LocationId, table.CountryId });
        }
    }
}
