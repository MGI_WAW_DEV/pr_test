﻿using CashManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CashManagement.Persistence.Configurations
{
    public class DeskConfiguration : IEntityTypeConfiguration<Desk>
    {
        public void Configure(EntityTypeBuilder<Desk> builder)
        {
            builder.HasKey(d => new { d.DeskNumber, d.AgencyNumber, d.OrganizationId});
            builder.Property(p => p.LastEmployeeId).HasColumnName("LastEmployee");
            builder.HasOne(d => d.LastEmployee).WithMany()
                .HasForeignKey(e => new { e.OrganizationId, e.LastEmployeeId });
            ;
            builder.ToTable("FX_DESKS");
        }
    }
}
