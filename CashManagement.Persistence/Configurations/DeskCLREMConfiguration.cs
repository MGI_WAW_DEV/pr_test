﻿using CashManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Persistence.Configurations
{
    public class DeskCLREMConfiguration : IEntityTypeConfiguration<DeskCLREM>
    {
        public void Configure(EntityTypeBuilder<DeskCLREM> builder)
        {
            builder.HasKey(d => new { d.DeskNumber, d.AgencyNumber, d.OrganizationId, d.Closed });
            builder.ToTable("FX_DESKCLREM");
        }
    }
}
