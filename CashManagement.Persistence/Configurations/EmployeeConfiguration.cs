﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CashManagement.Domain.Entities;

namespace CashManagement.Persistence.Configurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(e => new { e.OrganizationId, e.EmployeeNumber });

            builder.Property(e => e.Name).HasMaxLength(30);
            builder.Property(e => e.OrganizationId).HasMaxLength(15);

            builder.ToTable("FX_EMPLOYEE");

            
        }
    }
}
