﻿using CashManagement.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CashManagement.Persistence.Configurations
{
    public class DeskBalanceConfiguration : IEntityTypeConfiguration<DeskBalance>
    {
        public void Configure(EntityTypeBuilder<DeskBalance> builder)
        {
            builder.HasKey(d => new { d.DeskNumber, d.AgencyNumber, d.Currency, d.OrganizationId});
            builder.ToTable("FX_DESKBALANCE");
        }
    }
}
