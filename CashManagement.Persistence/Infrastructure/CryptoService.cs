﻿using System;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace CashManagement.Persistence.Infrastructure
{
    public interface ICryptoService
    {
        string Encrypt(string key, string plainText);
        string Decrypt(string key, string cipherText);
    }
    public class CryptoService : ICryptoService
    {
        public string Decrypt(string key, string cipherText)
        {
            var internals = GetInternalsFromKey(key);

            using (var cryptoServiceProvider = new DESCryptoServiceProvider())
            {
                using (var desDecrypt = cryptoServiceProvider.CreateDecryptor(internals.Key, internals.Iv))
                {
                    using (var stream = new MemoryStream())
                    {
                        using (var cryptoStream = new CryptoStream(stream, desDecrypt, CryptoStreamMode.Write))
                        {
                            var bytes = ConvertFromBase64(cipherText);

                            try
                            {
                                return DeriveStringFromBytes(bytes, cipherText.Length, cryptoServiceProvider, stream, cryptoStream);
                            }
                            catch (Exception ex)
                            {
                                throw new SecurityException("Password decryption failed. Please contact administrator", ex);
                            }
                        }
                    }
                }
            }
        }

        public string Encrypt(string key, string plainText)
        {
            /* Plain text can't exceed the available size value because encryption algorithm will blow up */
            if (plainText.Length > 92160)
            {
                throw new ArgumentException("Data String too large. Keep within 90Kb.");
            }

            plainText = string.Format("{0,5:00000}" + plainText, plainText.Length);

            var internals = GetInternalsFromKey(key);

            var keyBuffer = new byte[plainText.Length];
            Encoding.ASCII.GetBytes(plainText, 0, plainText.Length, keyBuffer, 0);

            using (var desService = new DESCryptoServiceProvider())
            {
                using (var desEncryptor = desService.CreateEncryptor(internals.Key, internals.Iv))
                {
                    using (var plainTextStream = new MemoryStream(keyBuffer))
                    {
                        using (var cryptoStream = new CryptoStream(plainTextStream, desEncryptor, CryptoStreamMode.Read))
                        {
                            using (var encryptedStream = new MemoryStream())
                            {
                                cryptoStream.CopyTo(encryptedStream);
                                return encryptedStream.Length == 0 ? string.Empty : Convert.ToBase64String(encryptedStream.GetBuffer(), 0, (int)encryptedStream.Length);
                            }
                        }
                    }
                }
            }
        }

        private static string DeriveStringFromBytes(byte[] bytes, int cipherTextLength, DESCryptoServiceProvider cryptoServiceProvider, MemoryStream stream, CryptoStream cryptoStream)
        {
            long lRead = 0;

            while (cipherTextLength >= lRead)
            {
                cryptoStream.Write(bytes, 0, bytes.Length);
                lRead = stream.Length + bytes.Length / cryptoServiceProvider.BlockSize * cryptoServiceProvider.BlockSize;
            }

            var result = Encoding.ASCII.GetString(stream.GetBuffer(), 0, (int)stream.Length);

            var strLen = int.Parse(result.Substring(0, 5));
            return result.Substring(5, strLen);
        }
        private static byte[] ConvertFromBase64(string data)
        {
            return Convert.FromBase64CharArray(data.ToCharArray(), 0, data.Length);
        }

        private static Internals GetInternalsFromKey(string key)
        {
            var keyBuffer = new byte[key.Length];
            Encoding.ASCII.GetBytes(key, 0, key.Length, keyBuffer, 0);

            using (var shaProvider = new SHA1CryptoServiceProvider())
            {
                var derivedHash = shaProvider.ComputeHash(keyBuffer);
                return new Internals(derivedHash.Take(8).ToArray(), derivedHash.Skip(8).Take(8).ToArray());
            }
        }

        private class Internals
        {
            public Internals(byte[] key, byte[] iv)
            {
                Key = key;
                Iv = iv;
            }

            public byte[] Key { get; private set; }
            public byte[] Iv { get; private set; }
        }


    }
}
