﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace CashManagement.Persistence.Infrastructure
{
    public class ConnectionString
    {
        private readonly SqlConnectionStringBuilder _builder;
        private readonly ICryptoService _dbEncryptor;
        private readonly string _encryptionKey;
        public ConnectionString(string connectionString, string encryptionKey)
        {
            _builder = new SqlConnectionStringBuilder(connectionString);
            _encryptionKey = encryptionKey;
            _dbEncryptor = new CryptoService();
        }

        public string Password
        {
            get { return _dbEncryptor.Decrypt(_encryptionKey, _builder.Password); }
        }

        public override string ToString()
        {
            return string.Format(@"Server={0};Database={1};Connect Timeout={2};User ID={3};Password={4}",
                _builder.DataSource, _builder.InitialCatalog, _builder.ConnectTimeout, _builder.UserID, Password);
        }
    }
}
