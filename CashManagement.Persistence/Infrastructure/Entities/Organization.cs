﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Persistence.Infrastructure.Entities
{
    public class Organization
    {
        public string OrganizationId { get; set; }
        public string TransDbName { get; set; }
        public string TransDbServerName { get; set; }
        public string TransDbUserId { get; set; }
        public string TransDbPassword { get; set; }
        public int ConnectTimeout { get; set; }
    }
}
