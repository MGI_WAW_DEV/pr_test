﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Persistence.Infrastructure.Entities
{
    public class LocationMapping
    {
        public string OrganizationId {get; set;}
        public string LocationId { get; set;}
        public string CountryId { get; set;}
        public string AgencyNumber { get; set; }
    }
}
