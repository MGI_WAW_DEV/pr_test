﻿using CashManagement.Common;
using CashManagement.Persistence.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashManagement.Persistence.Infrastructure
{
    public interface IOrganizationSelector
    {
        string GetOrganizationId();
        LocationMapping GetMapping();
    }

    public class OrganizationSelector : IOrganizationSelector
    {
        private readonly IUserClaimsProvider _userProvider;
        private readonly CoreDbContext _context;
        public OrganizationSelector(IUserClaimsProvider provider, CoreDbContext context)
        {
            _userProvider = provider;
            _context = context;
        }

        public LocationMapping GetMapping()
        {
            var locationId = _userProvider.GetClaimValue("LocationId");
            var countryId = _userProvider.GetClaimValue("CountryId");

            var mapping = _context.LocationMapping.FirstOrDefaultAsync(x => x.LocationId == locationId
            && x.CountryId == countryId).Result;

            if (mapping != null && mapping.OrganizationId != null)
            {
                return mapping;
            }
            throw new OrganizationForLocationAndCountryNotFound($"Organization not found for location {locationId} and country {countryId}");
        }

        public string GetOrganizationId()
        {
            return GetMapping().OrganizationId;
        }   
    }

    public class OrganizationForLocationAndCountryNotFound : Exception
    {
        public OrganizationForLocationAndCountryNotFound(string message) : base(message) { }
    }
}
