﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace CashManagement.Persistence.Infrastructure
{
    public static class DbConfig
    {
        private static readonly IDictionary<string, ConnectionString> ConnectionStringsCache;
        private const string GetOrganizationDetails = @"
                SELECT   
                    [OrganizationId],       
                    [TransDbName],        
                    [TransDbServerName],
                    [TransDbUserId],        
                    [TransDbPassword],        
                    [ConnectTimeout]       
                FROM 
                    [ORGANIZATION] WITH (NOLOCK)";

        static DbConfig()
        {
            ConnectionStringsCache = new ConcurrentDictionary<string, ConnectionString>();
        }

        public static void RegisterOrReset(string connectionString, string encryptionKey)
        {
            if (ConnectionStringsCache.Any())
                ConnectionStringsCache.Clear();
            InitializeOrganizationConfigurations(connectionString, encryptionKey);
        }

        private static void InitializeOrganizationConfigurations(string coreDbConnectionString, string encryptionKey)
        {
            using (var cnn = new SqlConnection(coreDbConnectionString))
            {
                using (var cmd = cnn.CreateCommand())
                {
                    cmd.CommandText = GetOrganizationDetails;
                    cmd.CommandType = System.Data.CommandType.Text;
                    cnn.Open();

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        var connectionString = string.Format(
                            "Server={0};Database={1};User ID={2};Password={3};Connect Timeout={4}",
                            reader["TransDbServerName"],
                            reader["TransDbName"],
                            reader["TransDbUserId"],
                            reader["TransDbPassword"],
                            reader["ConnectTimeout"]);

                        var connection = new ConnectionString(connectionString, encryptionKey);
                        var organizationId = reader["OrganizationId"].ToString();

                        if (organizationId != null)
                            ConnectionStringsCache.Add(organizationId, connection);
                    }
                }
            }
        }

        public static string GetOrganizationConnectionString(string organizationId)
        {
            ConnectionString connectionString;
            if (ConnectionStringsCache.TryGetValue(organizationId, out connectionString))
            {
                return connectionString.ToString();
            }
            throw new OrganizationConnectionStringNotFound($"Connection string not found for {organizationId}");
        }
    }

    public class OrganizationConnectionStringNotFound : DbException
    {
        public OrganizationConnectionStringNotFound(string message) : base(message) { }
    }
}
