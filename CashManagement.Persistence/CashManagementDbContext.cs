﻿using Microsoft.EntityFrameworkCore;
using CashManagement.Domain.Entities;
using CashManagement.Persistence.Infrastructure;

namespace CashManagement.Persistence
{
    public class CashManagementDbContext : CashManagementDbContextBase
    {
        public CashManagementDbContext(DbContextOptions<CashManagementDbContextBase> options, IOrganizationSelector selector)
            : base(selector == null ? options : GetConnectionString(selector))
        {
        }

        private static DbContextOptions<CashManagementDbContextBase> GetConnectionString(IOrganizationSelector selector)
        {
            var orgId = selector.GetOrganizationId();
            return SqlServerDbContextOptionsExtensions.
                UseSqlServer(new DbContextOptionsBuilder<CashManagementDbContextBase>(), DbConfig.GetOrganizationConnectionString(orgId)).Options;
        }
    }


    public class CashManagementDbContextBase : DbContext
    {
        public CashManagementDbContextBase(DbContextOptions<CashManagementDbContextBase> options)
            : base(options)
        {
        }

        public DbSet<Desk> Desks { get; set; }
        public DbSet<DeskOpenClose> DeskOpenClose { get; set; }
        public DbSet<DeskBalance> DeskBalance { get; set; }
        public DbSet<DeskCLREM> DeskCLREMs { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Event> Events { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(CashManagementDbContext).Assembly);
        }
    }
}
