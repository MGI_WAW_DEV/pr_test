﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CashManagement.Application.Interfaces;
using MediatR;

namespace CashManagement.Application.Partner
{
    public class GetPartnerQueryHandler : IRequestHandler<GetPartnerQuery, Domain.Entities.Partner>
    {
        private readonly IPartnerHierarchyDataProvider phProvider;

        public GetPartnerQueryHandler(IPartnerHierarchyDataProvider phProvider)
        {
            this.phProvider = phProvider;
        }
        public Task<Domain.Entities.Partner> Handle(GetPartnerQuery request, CancellationToken cancellationToken)
        {
            return phProvider.GetPartner(request.OracleAccountNumber);
        }
    }
}