﻿using MediatR;

namespace CashManagement.Application.Partner
{
    public class GetPartnerQuery : IRequest<Domain.Entities.Partner>
    {
        public string OracleAccountNumber { get; set; }
    }
}