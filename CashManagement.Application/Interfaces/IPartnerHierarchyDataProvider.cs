﻿using System.Threading.Tasks;
using CashManagement.Domain.Entities;

namespace CashManagement.Application.Interfaces
{
    public interface IPartnerHierarchyDataProvider
    {
        Task<Domain.Entities.Partner> GetPartner(string oracleAccountNumber);

    }
}