﻿using CashManagement.Application.Notifications.Models;
using System.Threading.Tasks;

namespace CashManagement.Application.Interfaces
{
    public interface INotificationService
    {
        Task SendAsync(Message message);
    }
}
