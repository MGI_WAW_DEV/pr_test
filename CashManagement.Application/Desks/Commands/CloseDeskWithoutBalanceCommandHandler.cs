﻿using CashManagement.Common;
using CashManagement.Domain.Entities;
using CashManagement.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CashManagement.Application.Desks.Commands
{
    public class CloseDeskWithoutBalanceCommandHandler : IRequestHandler<CloseDeskWithoutBalanceCommand, Unit>
    {
        private readonly CashManagementDbContext _context;
        private readonly IUserClaimsProvider _userProvider;
        private readonly IUserInfo _userInfo;
        private const string TimeStampFormat = "yyyyMMddHHmmss";

        public CloseDeskWithoutBalanceCommandHandler(CashManagementDbContext context, IUserClaimsProvider userProvider, IUserInfo userInfo)
        {
            _context = context;
            _userProvider = userProvider;
            _userInfo = userInfo;
        }

        public Task<Unit> Handle(CloseDeskWithoutBalanceCommand request, CancellationToken cancellationToken)
        {
            UpdateDesk(request);
            AddDeskRemarks(request);

            _context.SaveChanges();

            return null;
        }

        private void UpdateDesk(CloseDeskWithoutBalanceCommand request)
        {
            var entity = _context.Desks.FirstOrDefault(d => d.AgencyNumber == _userInfo.AgencyNumber
            && d.OrganizationId == _userInfo.OrganizationId
            && d.DeskNumber == request.DeskId);

            if (entity != null)
            {
                entity.LastRemarks = request.Remarks;
                entity.LastClosed = DateTime.UtcNow;
                entity.LastEmployeeId = _userInfo.EmployeeNumber;
                entity.CurrentEmployee = String.Empty;
            }
        }

        private void AddDeskRemarks(CloseDeskWithoutBalanceCommand request)
        {
            var deskCLREM = new DeskCLREM
            {
                AgencyNumber = _userInfo.AgencyNumber,
                EmployeeNumber = _userInfo.EmployeeNumber,
                DeskNumber = request.DeskId,
                OrganizationId = _userInfo.OrganizationId,
                Remarks = request.Remarks,
                Closed = DateTime.UtcNow
            };

            _context.DeskCLREMs.Add(deskCLREM);
        }

        private void AddCloseDeskEvent(CloseDeskWithoutBalanceCommand request)
        {
            var eventEntity = new Event
            {
                OrganizationId = _userInfo.OrganizationId,
                AgencyNumber = _userInfo.AgencyNumber,
                TimeKey = "",
                ContactNumber = string.Empty,
                EmployeeNumber = _userInfo.EmployeeNumber,
                DeskNumber = request.DeskId,
                TransTime = DateTime.UtcNow,
                EventID = 201,
                Report = request.Remarks,
                Reviewed = 0
            };
            _context.Events.Add(eventEntity);
        }
    }
}
