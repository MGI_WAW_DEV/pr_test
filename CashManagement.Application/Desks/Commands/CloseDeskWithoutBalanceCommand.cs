﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Application.Desks.Commands
{
    public class CloseDeskWithoutBalanceCommand : IRequest
    {
        public string DeskId { get; set; }
        public string Remarks { get; set; }
    }
}
