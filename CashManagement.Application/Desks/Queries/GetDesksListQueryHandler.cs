﻿using CashManagement.Common;
using CashManagement.Application.Desks.Models;
using CashManagement.Persistence;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CashManagement.Application.Desks.Queries
{
    public class GetDesksListQueryHandler : IRequestHandler<GetDesksListQuery, DesksListModel>
    {
        private readonly CashManagementDbContext _context;
        private readonly IUserInfo _userInfo;
        public GetDesksListQueryHandler(CashManagementDbContext context, IUserInfo userInfo)
        {
            _context = context;
            _userInfo = userInfo;
        }

        public async Task<DesksListModel> Handle(GetDesksListQuery request, CancellationToken cancellationToken)
        {
            var desks = new DesksListModel
            {
                Desks = await _context.Desks.Select(DeskModel.Projection)
                                            .Where(a=>a.AgencyNumber == _userInfo.AgencyNumber)
                                            .ToListAsync(cancellationToken)
            };
            return desks;
        }
    }
}
