﻿using CashManagement.Application.Desks.Models;
using MediatR;

namespace CashManagement.Application.Desks.Queries
{
    public class GetDesksListQuery : IRequest<DesksListModel>
    {
    }
}
