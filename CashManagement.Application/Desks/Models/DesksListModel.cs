﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Application.Desks.Models
{
    public class DesksListModel
    {
        public IList<DeskModel> Desks { get; set; }
    }
}
