﻿using CashManagement.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;

namespace CashManagement.Application.Desks.Models
{
    public class DeskModel
    {
        public string DeskNumber { get; set; }
        public string AgencyNumber { get; set; }
        public string Name { get; set; }
        public string CurrentEmployee { get; set; }
        public string OrganizationId { get; set; }
        public string LastClosedBy { get; set; }
        public DateTime LastClosed { get; set; }
        public string LastRemarks { get; set; }

        public static Expression<Func<Desk, DeskModel>> Projection
        {
            get
            {
                return desk => new DeskModel
                {
                    AgencyNumber = desk.AgencyNumber,
                    DeskNumber = desk.DeskNumber,
                    CurrentEmployee = desk.CurrentEmployee,
                    Name = desk.Name,
                    OrganizationId = desk.OrganizationId,
                    LastClosed = desk.LastClosed,
                    LastRemarks = desk.LastRemarks,
                    LastClosedBy = desk.LastEmployee == null ? null : desk.LastEmployee.Name
                };
            }
        }
    }
}
