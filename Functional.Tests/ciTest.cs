﻿using CashManagement.Application.Desks.Models;
using Functional.Tests.Infrastructure_Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;



namespace Functional.Tests
{
    public class CiTest
    {
        static HttpClient client = new HttpClient();
        static HttpProvider request = new HttpProvider();
        string someGlobalVar;
        
        //[Fact]
        //[Trait("TestCategory", "Checking")]
        //public async Task classAndInterface()
        //{
        //    var deskListUrl = "http://qmnacms8266:82/api/Desks/GetDesksList";
        //    HttpResponseMessage getResponse = await request.GetAsync(deskListUrl);

        //    var getJson = await getResponse.Content.ReadAsStringAsync();
        //    //jsonformat
        //    //{"desks":[
        //    //{ "deskNumber":"1","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":null,"lastClosed":"2019-01-14T12:59:03.3","lastRemarks":"bla bla bla"},
        //    //{ "deskNumber":"2","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":null,"lastClosed":"2019-01-09T08:25:00.703","lastRemarks":"trololo"},
        //    //{ "deskNumber":"3","agencyNumber":"AMIRA","name":"","currentEmployee":"HAAGH1","organizationId":"BELCORE","lastClosedBy":"MORMNOPQRSTUVWXYZ","lastClosed":"2013-10-17T15:49:36.167","lastRemarks":""},{"deskNumber":"4","agencyNumber":"AMIRA","name":"","currentEmployee":"MORENO2","organizationId":"BELCORE","lastClosedBy":"MORMNOPQRSTUVWXYZ","lastClosed":"2013-10-01T15:41:00.707","lastRemarks":""},{"deskNumber":"5","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":"HASBCDEFGHIJ","lastClosed":"2013-07-27T13:32:55.093","lastRemarks":""},{"deskNumber":"6","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":"MORMNOPQRSTUVWXYZ","lastClosed":"2013-09-13T13:07:29.617","lastRemarks":""}]}

        //    DesksListModel myDesks = JsonConvert.DeserializeObject<DesksListModel>(getJson);
        //    Trace.WriteLine(getJson);

        //    someGlobalVar = getJson;
        //}
        [Fact]
        [Trait("TestCategory", "Checking")]
        public void orderCheck()
        {
            Trace.WriteLine(someGlobalVar);
            Trace.WriteLine(4 % 2);
            Trace.WriteLine(5 % 2);
        }

        //private void Metoda(Func<string, decimal, int> parametr)
        //{
        //    parametr("c", 4.8m);
        //}

        [Theory]
        [InlineData(1)]
        [InlineData(123)]
        [Trait("TestCategory", "Checking")]
        public void CheckTheoryInput(int input)
        {
            //Action a = () => { };
            //Func<string, decimal, int> f = (string x, decimal y) => { return 1; };

            //int r = f("a", 1.0m);
            //f("b", 3.5m);

            //Metoda(f);

            var given = new List<Object>();

            given.Add(input);

            Assert.True(given.Count == 1);
        }

        [Theory]
        [InlineData("1", 1)]
        [InlineData("123", 3)]
        [InlineData("12", 2)]
        [Trait("TestCategory", "Checking")]
        public void CheckTheoryExpected(string i, int o)
        {
            var givenString = i;

            var actual = givenString.Length;

            Assert.Equal(o, actual);
        }
    }
}
