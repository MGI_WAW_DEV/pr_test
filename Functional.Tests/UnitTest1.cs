using CashManagement.Application.Desks.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using Xunit;


namespace Functional.Tests
{
    public class UnitTest1
    {
        [Fact]
        [Trait("Sort", "Checking")]
        //https://stackoverflow.com/questions/36425008/mocking-httpclient-in-unit-tests
        //https://docs.microsoft.com/pl-pl/dotnet/api/system.net.http.httpclient.getasync?view=netcore-2.2#System_Net_Http_HttpClient_GetAsync_System_String_
        //https://docs.microsoft.com/pl-pl/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client
        //https://codedaze.io/unit-testing-with-httpclient/
        public async System.Threading.Tasks.Task Test1Async()
        {
            HttpClient client = new HttpClient();

            //!!!!!!!!!! GET
            HttpResponseMessage getResponse = await client.GetAsync("http://qmnacms8266:82/api/Desks/GetDesksList");

            var getJson = await getResponse.Content.ReadAsStringAsync();
            //jsonformat
            //{"desks":[
            //{ "deskNumber":"1","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":null,"lastClosed":"2019-01-14T12:59:03.3","lastRemarks":"bla bla bla"},
            //{ "deskNumber":"2","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":null,"lastClosed":"2019-01-09T08:25:00.703","lastRemarks":"trololo"},
            //{ "deskNumber":"3","agencyNumber":"AMIRA","name":"","currentEmployee":"HAAGH1","organizationId":"BELCORE","lastClosedBy":"MORMNOPQRSTUVWXYZ","lastClosed":"2013-10-17T15:49:36.167","lastRemarks":""},{"deskNumber":"4","agencyNumber":"AMIRA","name":"","currentEmployee":"MORENO2","organizationId":"BELCORE","lastClosedBy":"MORMNOPQRSTUVWXYZ","lastClosed":"2013-10-01T15:41:00.707","lastRemarks":""},{"deskNumber":"5","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":"HASBCDEFGHIJ","lastClosed":"2013-07-27T13:32:55.093","lastRemarks":""},{"deskNumber":"6","agencyNumber":"AMIRA","name":"","currentEmployee":"","organizationId":"BELCORE","lastClosedBy":"MORMNOPQRSTUVWXYZ","lastClosed":"2013-09-13T13:07:29.617","lastRemarks":""}]}

            DesksListModel myDesks = JsonConvert.DeserializeObject<DesksListModel>(getJson);
                //dynamic myDesks = JsonConvert.DeserializeObject<dynamic>(json);
                //foreach (var item in myDesks)
                //{
                //    var test = item.AgencyNumber;
                //    var test2 = item.AgencyNumberasdfhuyasdf;
                //}

            var givenDeskNumber = "3";


            Assert.Equal("MORMNOPQRSTUVWXYZ", myDesks.Desks.FirstOrDefault(x => x.DeskNumber == givenDeskNumber).LastClosedBy);
            Assert.Equal("3", givenDeskNumber);
            Trace.WriteLine(myDesks.Desks.FirstOrDefault(x => x.DeskNumber == givenDeskNumber).LastClosedBy);


            //!!!!!!!!!! POST
            //var postContent = new FormUrlEncodedContent(new[]
            //{
            //    new KeyValuePair<string, string>("deskId", "string"),
            //    new KeyValuePair<string, string>("remarks", "string")
            //    //new KeyValuePair<string, string>("freeform", "231")
            //});
            //HttpResponseMessage postResponse = await client.PostAsync("http://qmnacms8266:82/api/Desks/CloseWithoutBalance", postContent);
            ////HttpResponseMessage postResponse = await client.PostAsync("https://httpbin.org/response", postContent); 
            //var postJson = await postResponse.Content.ReadAsStringAsync();

            //Trace.WriteLine(postJson);

            var values = new Dictionary<string, string>
                {
                   { "quantity", "8" },
                   { "orderId", "212" },
                   {"productId", "3000" }
                };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("https://httpbin.org/post", new StringContent("{ test: \"123\"}"));

            var responseString = await response.Content.ReadAsStringAsync();

            Trace.WriteLine(responseString);
        }

        

    }
}
