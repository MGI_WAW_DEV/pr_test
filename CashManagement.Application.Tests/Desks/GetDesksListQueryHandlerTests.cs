﻿using CashManagement.Application.Desks.Models;
using CashManagement.Application.Desks.Queries;
using CashManagement.Common;
using CashManagement.Domain.Entities;
using CashManagement.Persistence;
using CashManagement.Persistence.Infrastructure;
using Moq;
using Shouldly;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using System.Linq;

namespace CashManagement.Application.Tests.Desks
{
    public class GetDesksListQueryHandlerTests : TestBase
    {
        private readonly CashManagementDbContext _context;
        Mock<IUserInfo> _userProvider = new Mock<IUserInfo>();
        public GetDesksListQueryHandlerTests()
        {
            _context = InitAndGetDbContext();
            _userProvider.SetupGet(x => x.AgencyNumber).Returns("AGNUMBER");
        }

        [Fact]
        public async Task get_desks_list_should_be_4()
        {

            var sut = new GetDesksListQueryHandler(_context, _userProvider.Object);

            var result = await sut.Handle(new GetDesksListQuery(), CancellationToken.None);

            result.ShouldBeOfType<DesksListModel>();
            result.Desks.Count.ShouldBe(4);
        }

        [Fact]
        public async Task get_desks_list_should_return_2_desks_with_LastEmployee_filled()
        {
            var sut = new GetDesksListQueryHandler(_context, _userProvider.Object);

            var result = await sut.Handle(new GetDesksListQuery(), CancellationToken.None);

            result.Desks.Where(d => string.IsNullOrEmpty(d.LastClosedBy)).Count().ShouldBe(2);
        }



        private CashManagementDbContext InitAndGetDbContext()
        {
            var context = GetCashManagementDbContext();
            context.Employees.Add(new Employee() { EmployeeNumber = "1", Name = "Employee One", OrganizationId = "TEST" });
            context.Desks.AddRange(new[] {
                new Desk {DeskNumber = "1", AgencyNumber = "AGNUMBER",Name = "Name1",CurrentEmployee = "Employe3",CurrentAcceptTime  = new DateTime(2019, 1, 3, 10 , 02, 13), LastStatus = 0,LastRemarks  = "",OrganizationId = "TEST"},
                new Desk {DeskNumber = "2", AgencyNumber = "AGNUMBER",Name = "Name2",CurrentEmployee = "Employe1",CurrentAcceptTime  = new DateTime(2018, 12, 28, 17 ,42, 1), LastEmployeeId  = "1",LastClosed  = DateTime.Now,LastStatus = 0,LastRemarks  = "",OrganizationId = "TEST"},
                new Desk {DeskNumber = "3", AgencyNumber = "AGNUMBER",Name = "Name3",CurrentEmployee = "Employe1",CurrentAcceptTime  = new DateTime(2018, 11, 3, 12 ,3, 59) , LastEmployeeId  = "1",LastClosed  = DateTime.Now,LastStatus = 1,LastRemarks  = "",OrganizationId = "TEST"},
                new Desk {DeskNumber = "4", AgencyNumber = "AGNUMBER",Name = "Name4",CurrentEmployee = "Employe2",CurrentAcceptTime  = new DateTime(2018, 1, 3, 13 ,25, 0)  , LastEmployeeId  = "1wrong",LastClosed  = DateTime.Now,LastStatus = 0,LastRemarks  = "",OrganizationId = "TEST"}

            });
            
            context.SaveChanges();

            return context;
        }
    }
}
