﻿using CashManagement.Common;
using CashManagement.Persistence;
using CashManagement.Persistence.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Moq;
using CashManagement.Persistence.Infrastructure.Entities;

namespace CashManagement.Application.Tests.Infrastructure
{
    public class OrganizationSelector : TestBase
    {
        IOrganizationSelector _service;
        Mock<IUserClaimsProvider> userProvider = new Mock<IUserClaimsProvider>();

        public OrganizationSelector()
        {
            CoreDbContext dbContext = InitAndGetDbContext();
            _service = new Persistence.Infrastructure.OrganizationSelector(userProvider.Object, dbContext);
        }

        [Fact]
        public void get_organizationIdSuccess()
        {
            userProvider.Setup(x => x.GetClaimValue("LocationId")).Returns("TEST");
            userProvider.Setup(x => x.GetClaimValue("CountryId")).Returns("BEL");

            var orgId = _service.GetOrganizationId();

            Assert.Equal("BELCORE", orgId);
        }

        [Fact]
        public void wrong_location_should_throw_exception()
        {
            userProvider.Setup(x => x.GetClaimValue("LocationId")).Returns("WRONG");
            userProvider.Setup(x => x.GetClaimValue("CountryId")).Returns("WRONG");

            Assert.Throws<OrganizationForLocationAndCountryNotFound>(() => _service.GetOrganizationId());
        }

        private CoreDbContext InitAndGetDbContext()
        {
            var context = GetCoreDbContext();

            context.LocationMapping.AddRange(new[] {
                new LocationMapping {CountryId = "BEL", LocationId = "TEST", OrganizationId = "BELCORE",AgencyNumber = "AGTEST1"},
                new LocationMapping {CountryId = "BEL", LocationId = "TEST2", OrganizationId = "BELCORE",AgencyNumber = "AGTEST2"},
                new LocationMapping {CountryId = "FRA", LocationId = "TEST", OrganizationId = "FXFRA",AgencyNumber = "AGNUMBER"},
            });

            context.SaveChanges();

            return context;
        }
    }
}
