﻿using CashManagement.Persistence.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Xunit;

namespace CashManagement.Application.Tests.Infrastructure
{
    public class CryptoService
    {
        string encryptKey = "testencrypt123";
        ICryptoService _service = new Persistence.Infrastructure.CryptoService();

        [Fact]
        public void encrypt_decrypt_string_should_be_same()
        {
            var stringToEncrypt = "TestPassword";

            var encrypted = _service.Encrypt(encryptKey, stringToEncrypt);
            var decrypted = _service.Decrypt(encryptKey, encrypted);

            Assert.Equal(stringToEncrypt, decrypted);
        }

        [Fact]
        public void wrong_cipther_decrypt_should_throw_exception()
        {
            var decryptedString = "tooshort";
            Assert.Throws<CryptographicException>(() => _service.Decrypt(encryptKey, decryptedString));
        }
    }
}
