﻿using CashManagement.Persistence.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
namespace CashManagement.Application.Tests.Infrastructure
{
    public class DbContext
    {
        string conn = "Server=DMNACMS6729\\PWTD1;Database=CORE;User ID=CORE;Password=CORE";
        string encryptKey = "testencrypt123";
        string orgId = "BELCORE";


        public DbContext()
        {
            DbConfig.RegisterOrReset(conn, encryptKey);
        }

        [Fact]
        public void get_org_conn_string_should_be_found()
        {
            string conn = DbConfig.GetOrganizationConnectionString(orgId);

            Assert.NotNull(conn);
            Assert.Equal("Server=DMNACMS6729\\PWTD1;Database=BEL_CORE;Connect Timeout=30;User ID=BEL_CORE;Password=Bel_Core", conn);
        }

        [Fact]
        public void get_org_conn_string_should_throw_exception()
        {
            Assert.Throws<OrganizationConnectionStringNotFound>(() => DbConfig.GetOrganizationConnectionString("wrongId"));
        }
    }
}
