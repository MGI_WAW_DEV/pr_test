﻿using Microsoft.EntityFrameworkCore;
using System;
using CashManagement.Persistence;
using CashManagement.Persistence.Infrastructure;
using Moq;

namespace CashManagement.Application.Tests
{
    public class TestBase
    {
        public CashManagementDbContext GetCashManagementDbContext()
        {
            var builder = new DbContextOptionsBuilder<CashManagementDbContextBase>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());

            var dbContext = new CashManagementDbContext(builder.Options, null);
            dbContext.Database.EnsureCreated();

            return dbContext;
        }

        public CoreDbContext GetCoreDbContext()
        {
            var builder = new DbContextOptionsBuilder<CoreDbContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());

            var dbContext = new CoreDbContext(builder.Options);
            dbContext.Database.EnsureCreated();

            return dbContext;
        }
    }
}
