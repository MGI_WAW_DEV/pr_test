﻿using System;

namespace CashManagement.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
