﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Common
{
    public interface IUserClaimsProvider
    {
        string GetClaimValue(string key);
    }
}
