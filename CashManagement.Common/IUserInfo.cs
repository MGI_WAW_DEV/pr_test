﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashManagement.Common
{
    public interface IUserInfo
    {
        string OrganizationId { get; }
        string AgencyNumber { get; }
        string EmployeeNumber { get;  }
    }
}
