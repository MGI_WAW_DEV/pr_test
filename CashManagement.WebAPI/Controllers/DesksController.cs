﻿using CashManagement.Application.Desks.Commands;
using CashManagement.Application.Desks.Models;
using CashManagement.Application.Desks.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CashManagement.WebAPI.Controllers
{
    public class DesksController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<DesksListModel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetDesksList()
        {
            return Ok(await Mediator.Send(new GetDesksListQuery()));
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> CloseWithoutBalance([FromBody]CloseDeskWithoutBalanceCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        public async Task<IActionResult> CloseWithoutBalance([FromBody]CloseDeskCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
